import "./umbrella.css";
import UmbrellaImg from "./images/umbrella.jpg";
import RaincoatImg from "./images/raincoat.jpg";
import GumbootsImg from "./images/umbrella.jpg";
import GlovesImg from "./images/gloves.jpg";
import kidsImg from "./images/kids.jpg";
import RainwearImg from "./images/rainwear.jpg";
import KidsUmbrellaImg from "./images/kidsUmbrella.jpg";
import YellowCoat from "./images/yellowRaincoat.jpg";
import Blackboots from "./images/Blackboots.jpg";
import RedGloves from "./images/redGloves.jpg";
import RedCap from "./images/redCap.jpg";
import OrangeCover from "./images/orangeCover.jpg";

function Umbrella() {
  return (
    <div>
      <Header />
      <Collections />
      <Sale />
      <Bestsellers />
      <Copyright />
    </div>
  );
}
function Header() {
  return (
    <section className="sec1">
      <div className="sec1back">
        <div className="header">
          <nav>
            <div className="logo">
              <img  src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABRFBMVEX////gWVHivABIneMJM1cALlX4+fvhuQDgtwDivgDiwQDivQDhWE83l+E9meJDm+LgUlMAKFDjVkrnU0MAJE7gUFM8n+jeSkAAGEgAHUrT2N4AK1L37cnx9/3K4Pb+/PTu2IbpzV7WV1EAD0QAA0Hoy1SXw+2v0PH8+Onmxj3jqTN9lcjjlULz46rw3pqQjbncZGB8tura6vlhqOb87+754eBdb4Xo6+7Ax8+ut8FLYXqJlqVtfpEbPV87VG/08tNCUEVXSUxhQ1NcQlpIRWYLSXTn9v/irxDhjCvhZkTQYWefe6BmlNHjrinieE7Hcn/16b2eiKzjhUy2f5fs1HbiclGHu+vjnjrSa3Hlwy/AdojhZlDjkEerfJru2YzleHLrnprvsK3zyMbnhoH21NLecR2vZIL338/p2+Fpeo6ZpLA/WHNdylD4AAAIMUlEQVR4nO2ca1vTSBSASdIrTVoTqr2xglSgqLBCC8XS4q67q7uKisquq3uhAnvD//99Z5K0tGnm0qS5DJ73E0J8nnk5Z+acGTqZmwMAAAAAAAAAAAAAAACAa0h1r7dXjXoQQVK9W0vU7l5nxcNkIpFIHkY9jOBIYUGkmIp6IIEBhuIDhuLzBRjWktfasNF4/M23T5589/0PO0+/inows6WxuLm8ms0++/EnFWEY6vMXR7ea22vrO9dBdHFrNZtJp+X5l69u3pBsVGPh9dGtvKZpyvb6TtRD9EFjczWN5BDZzKub0hjGwkfllqIUCshze13IWDa2Niw7xPw96YbkxJDuI0VMQdOawkker2YGekjwwc0JP5yrC29sRSSZ17ZvRz1ofpa2cld6SPCtqyBi4d1QEUdSWRejjDRO0qN+FEGHIg7kWvyT1emHBCenIElRUeLvuOzwI83BK8U344rYMWoJCpuyw0/O3qMLIsX7DkUlX4jrmtPYcPrJuQzDD62okuKkoDWfRi3jxlbG6Ydy9GemoWS8dgYRO8YvVV0CiHL0PStHzTz9elJR0ZoxW3GOXQIo557RltGRKE4K4jCuRy01yombIFeOmoYfXYKIwvhL1FpDltwylDdHzTydWE+tRTUumdrIuQrKOU4/tJ4+dzVEO49YbK0W3f3k7AO+WWgG0W2xMSdjDEqj6xpjGvILSupDd0M0GSNX3CQKThFCchCRYsRLKlFQlqfwowUxYkWy4HQhpAUx0kQlzkFUC6fyIy+n0So+JqyiOITvpwshrolEQ0WLqGg0yILc7cwVhMbGpJCPpvTnyIK5l7ztzIgiOYZKQYlCcJUWQurRBcHwBTmISn47fME/KYJydmo/asFQoqgZi+RlFJ9dTB9C+loT/mqzRJmEKElfeRCUjHe0IIY9Fe/QclSWvYRQUldohko+1O0ipdTLXoqhBT1Nwy381Bz1mKSsNA01T5fpOZrxFkJWmirhHcA1qDnqOUmZaapoYR2jblAFPZV7C2rRx3kaUt2nlkJ5+m3FFeqvdMOwiiIjhF560gELdEGl0AxD8Ji+zEy99x0zJO+D7SCGUTE+0AU91woMbQsVWhDpxV721nUPUFcYaRpGEBmzUM498z4NUZoeRT4TKScXdgg9V0OMwZqIwS+nJyxD79XQNGRNRCUfdGPDEpTnf/MhyNgGW4rBCm4yDdN+piG7Iga+1rDWGY/b+xFDRmsadOtGO0C0DX3Ue4zh/PSJSxCDPFpkJ6mfem8aMpeaYNOUmaQ+Fxr66X4YacrqZ7zvfq8UWYIoiMEJsppufxsLC2ZXE2iaMk4vZL8djWnI7GqCLPqsbQUy/N2vofEHeyIG1psuMUOIejafgjyLqaIF9Ulb0qcuRg19LqVci2lw3fcWh6HfJEVBZBvmg/ozDXNfIec++DdcaDINC0Ed8GeZIcy9nIEhu1wEttSwk9R/seAqF0HVfHbb7bvvxnD03kE138wDjFmUQ66CGNRiyt5Y+N5ZmIY8BTGYvo2nWEz9IZNJmEf7SmDlgvrZBDtLfRd8rqOaoDpTdjmU5RX/htLk9YQJAiqIjL/dY9L+Fxr6R4cGhsFsgleZgrmM390hhn3cFqGh7/2vaXhUcLVChGGYTqczaTkrpzOZjPMWF/koUTWMEkaVVhCS+bWhqgRD54Eivkmr5ZVmUzG/KATWtq0it43l48WG+a+lxuLWSW5c0s1QRW7qaf/T2cVuK5Ww3zjQal2cfTp/qGLPScOxtg1foV1b37F3hF/t3F5rIstgDDfuHC85v9fY3Bi5DutsS7Hd6aez1uDpiXcqtM76pxOWI4bm5dmJDi11ezsYwwk9i5E7leOGRulh/2J8aK5vjbjoSyVj9P8NDbUC6b5lyNdplwb3Kkcab7Wk9ncnxkV6L8YukhwGctB6x+rmU8OqlcPG2yidX7g8Rnvzx8XpwNFqvQvadkzuBNkc4zDahkap33J9iP5uk91zK1lNw0I+8qskTvD1riz+66hK8mO/vaXVx3HEH2+LzaWuMU7S829vqKVzkh/P+2laKFeRoRbBh555WJbfGpLb/BvA8waeM6n0LoZ3ZG2W/+pTf873jqHzv2MrODf3D/3HnG9R+nd2AwqbL+A9UWAoPGAoPmAoPmAoPmAoPmAoPmAoPmAoPmAoPmAoPmAoPmAoPmAoPmAoPmAoPmAoPmAoPmAoPmAoPmAoPmAoPmAoPmAoPmAoPmAoPmAoPmAYY9pVrkGLa7hXL9cfcTwnrGG3goZdr7IfFNUwdRcPu7bH8aSghu2yOexD9pOpmmmYENMw0WM/mSryPhkvUnVz3EX2k/y/i5hhza5ym/ngfsXM504IY5otHVOxss988FLHD+oHIYxpthzwDvzQ/FUUeUpnvLCSL5FgPde2Jmydnc5xI2UtIMw0tWIt4EKDJqJZ55KModu1oijeNBymaYU+wbo675obQ3rWRCzSBr9vzUKe7i6GXFpBrFE6t7ZVNbk69DjSs8avE4t5yn5C0BDOzVXrdp4SFNs9u+muCDkLMV1roUzoPTeFfd3O0bJ41X7IIEjJ8oFzc9T+zyqYKMTdSMY2G9rJ5ECjdjAax2q3ots/0T9HNrxZ0K4NFBN6vXfwqNput6uPunpZH36bY5Mca9q9oQuyqVTK5XKlWLv6FmkVEohUp5IgUxd5Dg45KNcIfjqjpROGdqfu5qjXu6KdPpGpdsrF5JhesljpClvnXWlfHtYrRT2JqOnFSrnz6PrEb0hq/7LbOTz83Dm4ZJ/eAAAAAAAAAAAAAAAAAAAAAAAAfCH8DwX67PhspWwzAAAAAElFTkSuQmCC"
                alt=""/>
              <h2>
                <em>MonsoonMart</em>
              </h2>
            </div>
            <div className="left-float">
              <input type="text" placeholder="Search.." name="search" />
              <button type="submit">
                <i className="fa fa-search"></i>
              </button>
              <img className="icon" id="search" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTHeby__6inEsDRsLjlE4ZFOg0kNX5GGyiChg&usqp=CAU"
              />
            </div>
          </nav>
        </div>
      </div>
    </section>
  );
}
function Collection(props) {
  return (
    <div>
      <div className="flex-card">
        <h3>{props.name}</h3>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
        <button>Shop Now &gt;&gt;</button>
        <img src={props.src} />
      </div>
    </div>
  );
}

function Collections() {
  return (
    <div>
      <section>
        <center>
          <h1 className="sec2head">Collections</h1>
        </center>
        <div className="sec2">
          <Collection name="Umbrella" src={UmbrellaImg} />
          <Collection name="Raincoats" src={RaincoatImg} />
          <Collection name="Gumboots" src={GumbootsImg} />
          <Collection name="Rainwear" src={RainwearImg} />
          <Collection name="Gloves" src={GlovesImg} />
          <Collection name="For kids" src={kidsImg} />
        </div>
      </section>
    </div>
  );
}
function Sale() {
  return (
    <div className="sec3">
      <div className="content">
        <h1>
          Ready to Grab the Monsoon Sale? <br />
          Get{" "}
          <span style={{ color: "rgba(198, 146, 86, 1)" }}>min 50% off</span>
        </h1>
        <button>Wishlist now</button>
      </div>
    </div>
  );
}
function Bestseller(props) {
  return (
    <div className="cards">
      <h5>{props.name}</h5>
      <img src={props.src} alt="" />
      <p>{props.price}</p>
      <button>Add to cart</button>
    </div>
  );
}

function Bestsellers() {
  return (
    <div>
      <section className="sec4">
        <div>
          <center>
            <h1>
              <strong>Season's Bestsellers</strong>
            </h1>
          </center>
          <div className="bestsellDiv">
            <Bestseller name="kids Umbrella"  price="$40"  src={KidsUmbrellaImg}  />
            <Bestseller name="Yellow Raincoat" price="$70" src={YellowCoat} />
            <Bestseller name="GumBoots" price="$90" src={Blackboots} />
          </div>
          <div className="bestsellDiv">
            <Bestseller name="Gloves" price="$20" src={RedGloves} />
            <Bestseller name="Cap" price="$40" src={RedCap} />
            <Bestseller name="Backpack Cover" price="$90" src={OrangeCover} />
          </div>
        </div>
      </section>
      <marquee>
        Season's sale starts at midnight 2 !! Season's sale starts at midnight 2
        !! Season's sale starts at midnight 2 !!
      </marquee>
    </div>
  );
}
function Copyright() {
  return (
    <div>
      <section class="sec5">
        <center>© 2022 MonsoonMart. All rights reserved.</center>
      </section>
    </div>
  );
}
export default Umbrella;
